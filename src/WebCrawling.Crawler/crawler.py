from lxml import html
from bs4 import BeautifulSoup
import requests
from pymongo import MongoClient
import pymongo
import pprint
import threading
import asyncio
import time
import pika

mongoClient = MongoClient(
    "192.168.43.100", 
    27017, 
    username="AdminUserTest", 
    password="haslo1234",
    authSource='admin',
    authMechanism='SCRAM-SHA-256')
    
db = mongoClient["WebCrawlingDatabase"]
wykopArticlesCollectionName = "WykopArticlesCollection"
wykopArticlesCommentsCollectionName = "WykopArticlesCommentsCollectionName"


def main():
    subscribedPhrase = ""

    def start():
        clearWykopArticlesCollection()
        clearWykopArticlesCommentsCollection()

        print("Crawling started")

        rabbitMqThread = threading.Thread(target=rabbitMq)
        rabbitMqThread.start()
        
        nextPageUrl = crawlWykopPage("https://www.wykop.pl/najnowsze")
        while(True):
            nextPageUrl = crawlWykopPage(nextPageUrl)

    def rabbitMq():
        url = 'amqp://testUser:pass@192.168.43.162:5672/vhost'
        params = pika.URLParameters(url)

        connection = pika.BlockingConnection(params)
        channel = connection.channel()

        channel.queue_declare(queue='testUser-OnSubscriptionPhraseUpdate')

        channel.basic_consume(callback,
                        queue='testUser-OnSubscriptionPhraseUpdate',
                        no_ack=True)
        channel.start_consuming()

    def callback(ch, method, properties, body):
        nonlocal subscribedPhrase 
        subscribedPhrase = body.decode()
        print("subscribed phrase changed: " + subscribedPhrase)

    def sendWykopNewArticleNotification(url, articleTitle, dateTime):
        record = { 'url': url, 'articleTitle': articleTitle, 'dateTime': dateTime }
        print(record)
        header = {"Content-type": "application/json", "Accept": "text/plain"}
        requests.post(url="http://localhost:5000/api/notifications", json=record, headers=header)
        print("sent notification about new article: " + url)

    def crawlWykopPage(url):
        try:
            print("crawling: " + url)
            parsedPage = getParsedPage(url)
            itemsStream = parsedPage.find_all('ul', attrs={"id": "itemsStream"})[0].find_all('li')

            for article in itemsStream:
                try:
                    description = article.find_all('div', attrs={"class": "description"})[0]
                    
                    articleLink = description.a.get("href")
                    articleTitle = description.get_text()[8:]
                    dateTime = article.find_all('time')[0].get("datetime")

                    if articleLink.find("paylink") == -1:
                        thread = threading.Thread(target=handleWykopArticle, args = (articleLink, articleTitle, dateTime))
                        thread.start()  
                    
                except:
                    nothing = 0

            nextPageUrl = getWykopNextPageLink(parsedPage)
            return nextPageUrl
            
        except:
            print("error by parsing page: " + url)

            urlSplit = url.split("/")
            oldPageNumber = urlSplit[-2]
            nextPageNumber = str(int(oldPageNumber) + 1)
            nextPageUrl = url.replace(oldPageNumber, nextPageNumber)
            return nextPageUrl
            


    def handleWykopArticle(url, articleTitle, dateTime):
        saveWykopArticleDataToMongoDb(url, articleTitle, dateTime)
        
        # parsedArticleContentPage = getParsedPage(url)
        # crawlWykopArticleComments(parsedArticleContentPage, url)
        # print("saved article comments: "+ url)

    def crawlWykopArticleComments(parsedPage, url):
        commentsStream = parsedPage.find_all('ul', attrs={"class": "comments-stream"})[0]

        for comment in commentsStream:
            try:      
                mainComment = comment.div

                mainCommentAuthor = mainComment.div.div.a.get_text()
                mainCommentContentText = mainComment.div.find_all('div')[1].p.get_text()[14:-19:]
                mainCommentDateTime = mainComment.find_all('time')[0].get("datetime")
                mainCommentContentMedia = ""

                try: 
                    mainCommentContentMedia = mainComment.div.find_all('div')[1].div.find_all('a')[1].get('href')
                except:
                    nothing = 0

                commentObject = {
                    "url": url,
                    "author": mainCommentAuthor,
                    "contentText": mainCommentContentText,
                    "contentMedia": mainCommentContentMedia,
                    "dateTime": mainCommentDateTime,
                    "subComments": []
                }

                try: 
                    subComments = comment.find_all('ul', attrs={"class": "sub"})[0]

                    for subComment in subComments:

                        subCommentAuthor = subComment.div.div.div.a.get_text()
                        subCommentContentText = subComment.div.find_all('div')[2].p.get_text()[14:]
                        subCommentDateTime = subComment.find_all('time')[0].get("datetime")
                        subCommentContentMedia = ""

                        try: 
                            subCommentContentMedia = subComment.div.div.find_all('div')[1].div.a.get("href")
                        except:
                            nothing = 0

                        subCommentObject = {
                            "author": subCommentAuthor,
                            "contentText": subCommentContentText,
                            "contentMedia": subCommentContentMedia,
                            "dateTime": subCommentDateTime
                        }
                        commentObject["subComments"].append(subCommentObject)
                except:
                    nothing = 0   

                saveWykopArticleCommentToMongoDb(commentObject)

            except:
                nothing = 0

        try:
            nextPageLink = getWykopNextPageLink(parsedPage)
            parsedNextPage= getParsedPage(nextPageLink)

            crawlWykopArticleComments(parsedNextPage, url)
        except:
            nothing = 0

    def clearWykopArticlesCommentsCollection():
        collection = db[wykopArticlesCommentsCollectionName]
        collection.drop()

        print("collection cleared")

    def clearWykopArticlesCollection():
        collection = db[wykopArticlesCollectionName]
        collection.drop()

        collection = db[wykopArticlesCollectionName]
        db[wykopArticlesCollectionName].create_index([
            ("url", pymongo.DESCENDING)], 
            unique=True)
        print("collection cleared")

    def isWykopArticleCommentAlreadyInDatabase(comment):
        collection = db[wykopArticlesCommentsCollectionName]
        documentsCount = collection.count_documents({
            "url": comment["url"],
            "author": comment["author"],
            "contentText": comment["contentText"],
            "contentMedia": comment["contentMedia"]})

        if documentsCount > 0:
            return True

        return False

    def saveWykopArticleCommentToMongoDb(comment):
        try:
            collection = db[wykopArticlesCommentsCollectionName]
            if isWykopArticleCommentAlreadyInDatabase(comment) == False:
                collection.insert_one(comment)
            else:
                query = {
                    "url": comment["url"],
                    "author": comment["author"],
                    "contentText": comment["contentText"],
                    "contentMedia": comment["contentMedia"]}
                collection.update(query, comment)
        except:
            print("error")

    def saveWykopArticleDataToMongoDb(url, articleTitle, dateTime):  
        try:
            collection = db[wykopArticlesCollectionName]
            record = { "url": url, "articleTitle": articleTitle, "dateTime": dateTime }
            collection.insert_one(record)
            if subscribedPhrase != "" and subscribedPhrase in articleTitle:
                print(subscribedPhrase)
                sendWykopNewArticleNotification(url, articleTitle, dateTime)
        except:
            nothing = 0 


    def getWykopNextPageLink(parsedPage):
        nextPageLink = ""
        navigationButtons = parsedPage.find_all('div', attrs={"class": "wblock rbl-block pager"})[0].p
        for item in navigationButtons:
            if item.string == 'następna':
                nextPageLink = item.get("href")
        return nextPageLink

    def getParsedPage(url):
        page = requests.get(url)
        parsedPage = BeautifulSoup(page.content, "lxml")
        return parsedPage

    start()
main()