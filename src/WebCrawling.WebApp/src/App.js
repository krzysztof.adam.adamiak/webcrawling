import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './App.css';

import WykopDashboard from './features/wykop/Dashboard';
import FacebookDashboard from './features/facebook/Dashboard';
import TwitterDashboard from './features/twitter/Dashboard';

import { NavLink, Switch, Route, Redirect } from 'react-router-dom';
import { Layout, Menu, Icon, Row, Col } from 'antd';
import { Input } from 'antd';

const Search = Input.Search;
const { Header, Sider, Content, Footer } = Layout;

class App extends Component {

  state = {
    collapsed: true,
    searchedValue: "",
    searchTimestamp: new Date()
  };

  constructor(props) {
    super(props);

    this.onSearch = this.onSearch.bind(this);
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  collapseMenu = () => {
    this.setState({
      collapsed: true,
    });
  }

  onSearch(value) {
    this.setState({
      searchedValue: value,
      searchTimestamp: new Date()
    })
  }

  render() {
    return (
      <Layout className="layoutTest">
        <Sider
        style={{
          overflowY: 'auto'
        }}
          className="sider"
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          onClick={this.collapseMenu}>
            <Menu  theme="dark" defaultSelectedKeys={['1']} mode="inline">
              <Menu.Item key="1">
                <NavLink to="/wykop"> 
                  <Icon type="dashboard" />
                  <span>Wykop</span>
                </NavLink>
              </Menu.Item>
              <Menu.Item key="2">
                <NavLink to="/facebook">
                    <Icon type="user" />
                    <span>Facebook</span>
                </NavLink>
              </Menu.Item>
              <Menu.Item key="3">
                <NavLink to="/twitter">
                    <Icon type="mobile" />
                    <span>Twitter</span>
                </NavLink>
              </Menu.Item>
            </Menu>
            {/* HACK This is workaround, because enabling scroll eats one item on the bottom
            Sider with top:45px will eat one item, so we had to make empty div with
            height 45px to handle it */}
            <div style={{height: '45px'}} ></div>
        </Sider>
        <Layout className="mainContentLayout">
          <Header className="header">
            <Icon className="menuTrigger" onClick={this.toggle} type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />  
              <Search
                    className= "searchBox"
                    placeholder="input search text"
                    onSearch={value => this.onSearch(value)}
                    enterButton
                  />
          </Header>
          <Content className="content" onClick={this.collapseMenu}>
            <Switch>
              <Route exact path="/wykop" render={() => (<WykopDashboard searchedValue={this.state.searchedValue} searchTimestamp={this.state.searchTimestamp}/>)}/>
              <Route path="/facebook" component={() => (<FacebookDashboard searchedValue={this.state.searchedValue} searchTimestamp={this.state.searchTimestamp}/>)}/>            
              <Route path="/twitter" component={() => (<TwitterDashboard searchedValue={this.state.searchedValue} searchTimestamp={this.state.searchTimestamp}/>)}/>
              <Redirect from='/' to='/wykop'/>
            </Switch>
          </Content>
          <Footer className="footer" onClick={this.collapseMenu}>
            Footer content
          </Footer>
        </Layout>
      </Layout>
    );
  }
}


export default App;
