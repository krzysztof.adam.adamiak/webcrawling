import React, { Component } from 'react'
import { List, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller'

import './Style.css';

export default class ArticlesList extends Component {
  render() {
    return (
      <div className="infinite-container">       
      <InfiniteScroll
        pageStart={0}
        loadMore={this.props.loadMoreFunction}
        hasMore={!this.props.loading && this.props.hasMore}
        useWindow={true}
      >
        <List
          itemLayout="vertical"
          size="small"
          dataSource={this.props.data}
          renderItem={item => (
            <List.Item key={item.id}>
              <List.Item.Meta
                title={ 
                <div>
                  <span>{item.dateTime}  </span>
                  <a href={item.url}>Link</a>
                </div> }
              />
             {item.articleTitle}
            </List.Item>
          )}
        >
          {this.props.loading && this.props.hasMore && (
            <div className="loading-container">
              <Spin tip="Loading..." size="large"/>
            </div>
          )}
        </List>
      </InfiniteScroll>
    </div>
    )
  }
}
