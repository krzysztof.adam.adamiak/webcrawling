import axios from 'axios';

export default class ArticlesService {

    getSearchedArticlesNextPage(pageSize, lastRecordId, searchedValue) {
        return axios.get(
            "/articles/?pageSize="+pageSize+"&lastRecordId="+lastRecordId+"&searchedValue="+searchedValue)
            .then(response => response.data);
    }

    getSearchedArticlesFirstPage(pageSize, searchedValue) {
        return axios.get(
            "/articles/?pageSize="+pageSize+"&searchedValue="+searchedValue)
            .then(response => response.data);
    }

    getAllArticlesNextPage(pageSize, lastRecordId) {
        return axios.get(
            "/articles/?pageSize="+pageSize+"&lastRecordId="+lastRecordId)
            .then(response => response.data);
    }

    getAllArticlesFirstPage(pageSize) {
        return axios.get(
            "/articles/?pageSize="+pageSize)
            .then(response => response.data);
    }

    changeSearchedPhrase(searchedPhrase) {
        axios.put("notifications?newSubscribedPhrase="+searchedPhrase);
    }

    clearSearchedPhrase() {
        axios.put("notifications/clear");
    }
}