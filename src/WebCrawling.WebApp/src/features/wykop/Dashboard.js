import React, { Component } from 'react'

import ArticlesService from './ArticlesService'
import ArticlesList from './ArticlesList'

import {  notification } from 'antd';
import { SignalRService } from '../core/SignalRService'

class Dashboard extends Component {
  
    state = {
        articlesList: [],
        hasArticlesListMoreData: true,
        loadingArticlesList: false,
      }
    
    constructor(props) {
        super(props);

        this.articlesService = new ArticlesService();
        this.pageSize = 20;

        this.loadMoreArticles = this.loadMoreArticles.bind(this);
        this.signalRService = new SignalRService("http://localhost:5000/messageHub");

        this.newArticleReceived = this.newArticleReceived.bind(this);
    }
  
    componentDidMount() {
      this.signalRService.start();
  
      this.signalRService
        .getConnection()
        .on("NewArticleReceived", (article) => this.newArticleReceived(article));
    }
  
    newArticleReceived(article) {
      // const articleJson = JSON.parse(article);
  
      console.log(article)
  
      const args = {
        message: article.url,
  
        description: ""
          + article.dateTime
          + " " 
          + article.articleTitle,
      };
      notification.warning(args);

    //   let newArticlesList = this.state.articlesList;

    //   newArticlesList.unshift(article)

    //   this.setState({
    //       articlesList: newArticlesList
    //   })
    }

    componentDidUpdate(prevProps, prevState) {
      if(prevProps.searchTimestamp !== this.props.searchTimestamp) {

        this.setState({
            articlesList:[],
            hasArticlesListMoreData: true
        })

        console.log(this.state.articlesList)
        console.log("|||"+this.props.searchedValue+"|||")

        if(this.props.searchedValue === "") {
            this.loadAllArticles(undefined);
            this.articlesService.clearSearchedPhrase();
        }
        else {
            this.loadSearchedArticles(undefined);
            this.articlesService.changeSearchedPhrase(this.props.searchedValue);
        }
      }
    }
    
    loadMoreArticles() {
        let lastArticle = this.state.articlesList[this.state.articlesList.length-1];

        if(this.props.searchedValue === "") {
            this.loadAllArticles(lastArticle);
        }
        else {
            this.loadSearchedArticles(lastArticle);
        }
    }

    loadAllArticles(lastArticle) {
        this.setState({
            loadingArticlesList: true
        })

        if(lastArticle === undefined)
        {
            this.articlesService.getAllArticlesFirstPage(
                this.pageSize)
                .then(response => {
                    if(response.length === 0) {
                        this.setState({ 
                            hasArticlesListMoreData: false,
                            loadingArticlesList: false
                            });
                    }
                    else {
                        this.setState({ 
                            articlesList: response,
                            loadingArticlesList: false
                        })
                    }
                })
        }
        else
        {
            this.articlesService.getAllArticlesNextPage(
                this.pageSize,
                lastArticle.id)
                .then(response => {
                    if(response.length === 0) {
                        this.setState({ 
                            hasArticlesListMoreData: false,
                            loadingArticlesList: false
                            });
                        }
                    else {
                        let newArticlesList = this.state.articlesList.concat(response);
                        this.setState({
                            articlesList: newArticlesList,
                            loadingArticlesList: false
                        })
                    }
                })
        }   
    }

    loadSearchedArticles(lastArticle) {
        this.setState({
            loadingArticlesList: true
        })

        if(lastArticle === undefined)
        {
            this.articlesService.getSearchedArticlesFirstPage(
                this.pageSize,
                this.props.searchedValue)
                .then(response => {
                    if(response.length === 0) {
                        this.setState({ 
                            hasArticlesListMoreData: false,
                            loadingArticlesList: false
                            });
                    }
                    else {
                        this.setState({ 
                            articlesList: response,
                            loadingArticlesList: false
                        })
                    }
                })
        }
        else
        {
            this.articlesService.getSearchedArticlesNextPage(
                this.pageSize,
                lastArticle.id,
                this.props.searchedValue)
                .then(response => {
                    if(response.length === 0) {
                        this.setState({ 
                            hasArticlesListMoreData: false,
                            loadingArticlesList: false
                            });
                        }
                    else {
                        let newArticlesList = this.state.articlesList.concat(response);
                        this.setState({
                            articlesList: newArticlesList,
                            loadingArticlesList: false
                        })
                    }
                })
        }   
    }
  
    render() {
        return (
            <div>
                <ArticlesList 
                data={this.state.articlesList}
                loadMoreFunction={this.loadMoreArticles}
                hasMore={this.state.hasArticlesListMoreData}
                loading={this.state.loadingArticlesList}/>
            </div>
        )
    }
}

export default Dashboard;