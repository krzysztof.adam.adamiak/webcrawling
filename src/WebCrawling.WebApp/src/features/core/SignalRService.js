
const signalR = require("@aspnet/signalr"); 

export class SignalRService  {

    connection;

    constructor(url) {
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl(url)
            .configureLogging(signalR.LogLevel.Information)
            .build();

        this.connection.onclose(async () => {
            await this.start();       
        });

        console.log(this.connection);
    }

    async start() {
        try {
            await this.connection.start();
            console.log("SignalR connection OK");
        } catch (err) {
            console.log("SignalR connection FAIL\n" + err);
                setTimeout(() => this.start(), 1000);
            
        }
    };

    async stop() {
        try {
            await this.connection.stop();
            console.log("SignalR polling stopped");
        } catch (err) {
            console.log("SignalR polling stopping FAIL\n" + err);
        }
    };

    getConnection() {
        return this.connection;
    };
}

export default SignalRService
