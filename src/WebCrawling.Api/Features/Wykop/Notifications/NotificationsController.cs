﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;
using WebCrawling.Api.Features.Wykop.Articles;

namespace WebCrawling.Api.Features.Wykop.Notifications
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController
    {
        private readonly NotificationsService notificationsService;

        public NotificationsController(NotificationsService notificationsService)
        {
            this.notificationsService = notificationsService;
        }

        [HttpPut]
        public IActionResult UpdateSubscribedPhrase(string newSubscribedPhrase, CancellationToken cancellationToken)
        {
            this.notificationsService.HandleUpdateSubscription(newSubscribedPhrase, cancellationToken);
            return new OkResult();
        }

        [HttpPut]
        [Route("clear")]
        public IActionResult ClearSubscribedPhrase(CancellationToken cancellationToken)
        {
            this.notificationsService.ClearSearchedPhrase(cancellationToken);
            return new OkResult();
        }

        [HttpPost]
        public async Task<IActionResult> PostNotification(ArticleModel newArticle, CancellationToken cancellationToken)
        {
            await this.notificationsService.HandleNotificationAsync(newArticle, cancellationToken);
            return new OkResult();
        }
    }
}
