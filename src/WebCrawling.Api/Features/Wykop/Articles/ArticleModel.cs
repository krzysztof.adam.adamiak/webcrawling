﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace WebCrawling.Api.Features.Wykop.Articles
{
    public class ArticleModel
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("url")]
        public string Url { get; set; }

        [BsonElement("articleTitle")]
        public string ArticleTitle { get; set; }

        [BsonElement("dateTime")]
        public string DateTime { get; set; }
    }
}