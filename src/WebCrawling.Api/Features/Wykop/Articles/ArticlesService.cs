﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Milsa.MyCare.EventHub.Features.MongoDb;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WebCrawling.Api.Features.Wykop.Articles
{
    public class ArticlesService
    {
        private readonly MongoDbService mongoDbService;
        private readonly ILogger<ArticlesService> logger;
        private readonly string collectionName = "WykopArticlesCollection";

        public ArticlesService(MongoDbService mongoDbService, ILogger<ArticlesService> logger)
        {
            this.mongoDbService = mongoDbService;
            this.logger = logger;
        }

        public async Task<List<ArticleModel>> GetAllArticlesFirstPage(int pageSize, CancellationToken cancellationToken)
        {
            return await this.mongoDbService
                .MongoDbClient
                .GetCollection<ArticleModel>(this.collectionName)
                .Find(_ => true)
                .SortBy(item => item.Id)
                .Limit(pageSize)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<ArticleModel>> GetAllArticlesNextPage(int pageSize, string lastRecordId, CancellationToken cancellationToken)
        {
            return await this.mongoDbService
                .MongoDbClient
                .GetCollection<ArticleModel>(this.collectionName)
                .Find(article => article.Id > ObjectId.Parse(lastRecordId))
                .SortBy(item => item.Id)
                .Limit(pageSize)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<ArticleModel>> GetSearchedArticlesFirstPage(int pageSize, string searchedValue, CancellationToken cancellationToken)
        {
            return await this.mongoDbService
                .MongoDbClient
                .GetCollection<ArticleModel>(this.collectionName)
                .Find(article => article.ArticleTitle.ToLower().Contains(searchedValue.ToLower()))
                .SortBy(item => item.Id)
                .Limit(pageSize)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<ArticleModel>> GetSearchedArticlesNextPage(int pageSize, string lastRecordId, string searchedValue, CancellationToken cancellationToken)
        {
            return await this.mongoDbService
                .MongoDbClient
                .GetCollection<ArticleModel>(this.collectionName)
                .Find(article 
                    => article.Id > ObjectId.Parse(lastRecordId)
                    && article.ArticleTitle.ToLower().Contains(searchedValue.ToLower()))
                .SortBy(item => item.Id)
                .Limit(pageSize)
                .ToListAsync(cancellationToken);
        }
    }
}
