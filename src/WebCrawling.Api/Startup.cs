﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Milsa.MyCare.EventHub.Features.MongoDb;
using WebCrawling.Api.Features.SignalR;
using WebCrawling.Api.Features.Wykop.Articles;
using WebCrawling.Api.Features.Wykop.Notifications;

namespace WebCrawling.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ArticlesService>();

            services.AddSingleton<NotificationsService>();
            services.Configure<NotificationsServiceOptions>(Configuration.GetSection("NotificationsService"));

            services.AddSingleton<MongoDbService>();
            services.Configure<MongoDbServiceOptions>(Configuration.GetSection("MongoDbService"));

            services.AddCors();

            services.AddSignalR();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials());

            app.UseSignalR(routes =>
            {
                routes.MapHub<MessageHub>("/messageHub");
            });

            app.UseMvc();
        }
    }
}
